# Slicet

[See the demo here](https://foxnewsnetwork.gitlab.io/slicet/)

## Some notes

1. all the files I used to pass this test lives in `tests/dummy/app`, everything else is "framework"
2. the "server-side" code is mocked on the client-side via ye olde' `pretender.js`. I just copy+pasted the details in your original `routes/route.js` file into `tests/dummy/app/mocks/qa.js`
3. the only "edits" I permitted myself to make on the "server-side" code are related to passing jshint, but the actual format of the "upstream" data is unchanged
4. the pictures of the apples and oranges don't show up because the server decided to pass down `src` urls that begin with a `/`, but I don't have a spare domain name to point to this worktest project to get proper asset hosting from gitlab (oh well)
5. the "solution code" lives on the `pages` branch in `public`

## Installation

* `git clone` this repository
* `npm install`
* `bower install`

## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `npm test` (Runs `ember try:testall` to test your addon against multiple Ember versions)
* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://ember-cli.com/](http://ember-cli.com/).
