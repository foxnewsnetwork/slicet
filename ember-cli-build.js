/*jshint node:true*/
/* global require, module */
var EmberAddon = require('ember-cli/lib/broccoli/ember-addon');

module.exports = function(defaults) {
  var app = new EmberAddon(defaults, {
    babel: { includePolyfill: true },
    pretender: true,
    sassOptions: {
      includePaths: [
        'bower_components/material-design-lite/src'
      ]
    }
  });

  /*
    This build file specifies the options for the dummy test app of this
    addon, located in `/tests/dummy`
    This build file does *not* influence how the addon or the app using it
    behave. You most likely want to be modifying `./index.js` or app's build file
  */
  app.import(app.bowerDirectory + '/material-design-lite/material.js');
  app.import(app.bowerDirectory + '/FakeXMLHttpRequest/fake_xml_http_request.js');
  app.import(app.bowerDirectory + '/route-recognizer/dist/route-recognizer.js');
  app.import(app.bowerDirectory + '/pretender/pretender.js');
  return app.toTree();
};
