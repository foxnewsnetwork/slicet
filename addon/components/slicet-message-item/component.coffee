`import Ember from 'ember'`
`import layout from './template'`

SlicetMessageItemComponent = Ember.Component.extend
  layout: layout
  tagName: "li"
  classNames: ["mdl-list__item", "slicet-message-item"]
  avatarPath: "picture"

`export default SlicetMessageItemComponent`
