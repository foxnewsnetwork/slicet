`import Ember from 'ember'`
`import layout from './template'`

SlicetMessagesComponent = Ember.Component.extend
  layout: layout
  tagName: "ul"
  classNames: ["mdl-list", "slicet-messages"]

`export default SlicetMessagesComponent`
