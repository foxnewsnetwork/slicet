`import Ember from 'ember'`
`import layout from './template'`

SlicetCenterHeroComponent = Ember.Component.extend
  layout: layout
  classNames: ["slicet-center-hero"]

`export default SlicetCenterHeroComponent`
