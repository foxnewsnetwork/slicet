`import Ember from 'ember'`
`import layout from './template'`

SlicetQuestionCardComponent = Ember.Component.extend
  layout: layout
  classNames: ["slicet-question-card"]

`export default SlicetQuestionCardComponent`
