`import qaMocker from '../mocks/qa'`

initialize = (application) ->
  server = new Pretender qaMocker
  application.register "pretender:server", server, instantiate: false

PretendServerInitializer =
  name: 'pretend-server'
  initialize: initialize

`export {initialize}`
`export default PretendServerInitializer`
