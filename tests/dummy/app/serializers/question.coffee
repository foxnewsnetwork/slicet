`import DS from 'ember-data'`

QuestionSerializer = DS.RESTSerializer.extend DS.EmbeddedRecordsMixin,
  primaryKey: "qid"
  attrs:
    user:
      embedded: "always"
`export default QuestionSerializer`