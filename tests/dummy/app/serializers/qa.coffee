`import DS from 'ember-data'`

QaSerializer = DS.RESTSerializer.extend DS.EmbeddedRecordsMixin,
  primaryKey: "qaId"
  attrs:
    user:
      embedded: "always"
    questions:
      deserialize: "records"
  normalizeFindRecordResponse: (store, primaryModelClass, payload, id, requestType) ->
    @_super store, primaryModelClass, {qa: payload}, id, requestType
  normalizeArrayResponse: (store, primaryModelClass, {results}, id, requestType) ->
    @_super store, primaryModelClass, {qas: results}, id, requestType
`export default QaSerializer`