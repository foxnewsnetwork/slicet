`import DS from 'ember-data'`

Qa = DS.Model.extend
  topic: DS.attr "string"
  user: DS.belongsTo "user", async: false
  questions: DS.hasMany "question", async: false
`export default Qa`