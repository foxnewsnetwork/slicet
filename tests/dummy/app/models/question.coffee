`import DS from 'ember-data'`

Question = DS.Model.extend
  text: DS.attr "string"
  answer: DS.attr "string"
  user: DS.belongsTo "user", async: false

`export default Question`