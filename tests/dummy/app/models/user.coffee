`import DS from 'ember-data'`

User = DS.Model.extend
  name: DS.attr "string"
  picture: DS.attr "string"
  location: DS.attr "string"

`export default User`