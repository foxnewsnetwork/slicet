// macro to handle the slight variation in DSL between express and pretender
const resMacro = {
  status (code) {
    return {
      send (response) {
        return [code, {"Content-Type": "application/json"}, JSON.stringify(response)];
      }
    };
  }
};

// Copy + pasted from the routes.js
// We do this because we need to mock a server on the front end
export default function () {
  const router = this;  
  router.get('/api/qa', () => {
    var mockResponse = {
      results: [
        {
          qaId: 0,
          topic: 'How \'bout them apples?',
          user: {
            id: 7,
            name: 'Apple',
            picture: '/assets/apple.png'
          }
        },
        {
          qaId: 1,
          topic: 'Fruit ninja',
          user: {
            id: 11,
            name: 'Avocado',
            picture: '/assets/avocado.png'
          }
        }
      ]
    };

    return resMacro.status(200).send(mockResponse);
  });

  /**
   * GET /api/qa/:id
   * @returns a single Q&A session
   */
  router.get('/api/qa/:id', (req) => {
    var id = ''+req.params.id;

    var mockResponse = {};

    if(id === '0') {
      mockResponse = {
        qaId: 0,
        topic: 'How \'bout them apples?',
        user: {
          id: 7,
          name: 'Apple',
          picture: '/assets/apple.png'
        },
        questions: [
          {
            qid: 0,
            text: 'Why are you so delicious?',
            user: {
              id: 8,
              name: 'Banana',
              picture: '/assets/banana.png',
              location: 'Palo Alto, US'
            },
            answer: 'I\'m full of fructose!'
          },
          {
            qid: 1,
            text: 'What is your nutritional value?',
            user: {
              id: 10,
              name: 'Grape',
              picture: '/assets/grape.png',
              location: 'Seattle, US'
            },
            answer: 'Great question! I\'m packed with Vitamin C.'
          }
        ]
      };

      return resMacro.status(200).send(mockResponse);
    } else if(id === '1') {
      mockResponse = {
        qaId: 1,
        topic: 'Fruit ninja',
        user: {
          id: 11,
          name: 'Avocado',
          picture: '/assets/avocado.png'
        },
        questions: [
          {
            qid: 3,
            text: 'What\'s your high score?',
            user: {
              id: 10,
              name: 'Grape',
              picture: '/assets/grape.png',
              location: 'Seattle, US'
            },
            answer: 'Eleventy million!!'
          },
          {
            qid: 4,
            text: 'Are you even a fruit?',
            user: {
              id: 8,
              name: 'Banana',
              picture: '/assets/banana.png',
              location: 'Palo Alto, US'
            },
            answer: 'I plead the fif.'
          }
        ]
      };

      return resMacro.status(200).send(mockResponse);
    } else {
      mockResponse = {
        error: 'Not found'
      };

      return resMacro.status(404).send(mockResponse);
    }
  });
}
