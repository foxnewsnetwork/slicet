import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route("qa", {path: "/qa/:qa_id"});
  this.route("jobs");
  this.route("contacts");
});

export default Router;
