`import DS from 'ember-data'`

ApplicationAdapter = DS.RESTAdapter.extend
  namespace: "api"
  pathForType: (modelName) -> 
    switch modelName
      when "qas", "qa" then "qa"
      else @_super modelName

`export default ApplicationAdapter`